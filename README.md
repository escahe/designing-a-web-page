# **Web Page: Store Catalogue**

## **Requirements:**

### * The system must:

* Display a list of products with their names, images, descriptions, and prices.
* Implement a filter or search
* Functionality to narrow down the products.
* Allow users to click on a product for more details.
* Create a cart functionality to add products.

### * The system should be:

* Responsive design for various screen sizes.
* Clear and intuitive user interface.
* Efficient loading of product images and data

## Page Design

![1702932209313](image/README/1702932209313.png)
