// Function to fetch products from FakeStoreAPI
async function fetchProducts() {
    try {
      const response = await fetch('https://fakestoreapi.com/products');
      const products = await response.json();
      return products;
    } catch (error) {
      console.error('Error fetching products:', error);
      return [];
    }
  }

// Function to display products
function displayProducts(products) {
  const productList = document.getElementById('productList');
  productList.innerHTML = '';

  products.forEach(product => {
    const productCard = document.createElement('div');
    productCard.classList.add('product');

    const productImage = document.createElement('img');
    productImage.src = product.image;
    productImage.alt = product.title;
    productImage.classList.add('product-image'); // Add a class for styling
    productCard.appendChild(productImage);

    const productName = document.createElement('h3');
    productName.textContent = product.title;
    productCard.appendChild(productName);

    const productPrice = document.createElement('p');
    productPrice.textContent = `$${product.price}`;
    productCard.appendChild(productPrice);

    productList.appendChild(productCard);
  });
}
  
// Fetch products and display them when the page loads
window.addEventListener('load', async () => {
  const products = await fetchProducts();
  displayProducts(products);
});
  